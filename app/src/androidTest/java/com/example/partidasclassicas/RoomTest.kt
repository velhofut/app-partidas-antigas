package com.example.partidasclassicas

import android.content.Context
import android.util.Log
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class RoomTest {
    private lateinit var matchDAO: MatchDAO
    private lateinit var roomDB: BancoPrehFeito

    @Before
    fun criaBD() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        roomDB = BancoPrehFeito.getInstance(context)
        //roomDB = Room.inMemoryDatabaseBuilder(
        //    context, BancoPrehFeito::class.java).build()
        matchDAO = roomDB.matchDao()
    }

    @After
    @Throws(IOException::class)
    fun fechaBD() {
        roomDB.close()
    }

    @Test
    @Throws(Exception::class)
    fun testaAcessoBD() {

        val listaMatches1: List<Match> = roomDB.matchDao().pegaPartidaDeID(3)
        val matchEncontrado = listaMatches1[0]
        assertEquals(matchEncontrado.country_id, 1)
        assertEquals(matchEncontrado.match_api_id, 492475)

        val listaMatches2 = roomDB.matchDao().pegaPartidaDeID(30001)
        assertEquals(listaMatches2.size, 0)

        val listaMatches3 = roomDB.matchDao().pegaPartidaDeID(1)
        val match3 = listaMatches3[0]
        assertEquals(match3.season, "2008/2009")
        assertEquals(match3.home_team_goal, 1)
        assertEquals(match3.league_id, 1)
        assertEquals(match3.date, "2008-08-17 00:00:00")

        val listaTemporadas: List<String> = roomDB.matchDao().listaTemporadas()
        assertEquals(listaTemporadas[0], "2008/2009")
        assertEquals(listaTemporadas[7], "2015/2016")

        val listaPartidas: List<Match> = roomDB.matchDao().pegaPartidaComNomeETemporada(9987, "2008/2009")
        assertEquals(listaPartidas[0].away_team_api_id, 9993)
        assertEquals(listaPartidas[33].away_team_goal, 2)

        val listaEquipes = roomDB.teamDao().pegaTodosNomes()
        assertEquals(listaEquipes.size, 299)
        assertEquals(listaEquipes[101], "1. FC Köln")

        }

}