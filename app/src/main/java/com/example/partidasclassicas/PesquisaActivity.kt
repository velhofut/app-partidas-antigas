package com.example.partidasclassicas

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import androidx.activity.ComponentActivity


class PesquisaActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pesquisa_activity)

        val botao = findViewById<Button>(R.id.botao_exibe_partidas)
        val bancoDados: BancoPrehFeito = BancoPrehFeito.getInstance(applicationContext)

        val listaEquipes: List<String> = bancoDados.teamDao().pegaTodosNomes()
        val listaTemporadas: List<String> = bancoDados.matchDao().listaTemporadas()

        val adaptadorTemporadas: ArrayAdapter<String> =
            ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, listaTemporadas)
        val adaptadorNomes: ArrayAdapter<String> =
            ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, listaEquipes)

        val pesquisaNome = findViewById<AutoCompleteTextView>(R.id.pesquisa_nome)
        pesquisaNome.setThreshold(5)
        pesquisaNome.setAdapter(adaptadorNomes)
        val pesquisaTemporada = findViewById<AutoCompleteTextView>(R.id.pesquisa_temporada)
        pesquisaTemporada.setAdapter(adaptadorTemporadas)

        botao.setOnClickListener {
            val nomeEquipe = pesquisaNome.text.toString()
            val temporada = pesquisaTemporada.text.toString()

            val intent = Intent(this@PesquisaActivity, ResultadoPesquisa::class.java)
            intent.putExtra("Nome_Equipe", nomeEquipe)
            intent.putExtra("Temporada", temporada)
            startActivity(intent)
        }
    }
}
