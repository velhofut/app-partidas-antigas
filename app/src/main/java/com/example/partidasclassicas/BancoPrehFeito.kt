package com.example.partidasclassicas
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import androidx.room.Database

@Database(entities = [Match::class, Team::class, Player::class, Country::class, League::class], version = 1)
abstract class BancoPrehFeito: RoomDatabase() {
    abstract fun matchDao(): MatchDAO
    abstract fun teamDao(): TeamDAO
    companion object {
        @Volatile
        private var INSTANCE: BancoPrehFeito? = null

        fun getInstance(context: Context): BancoPrehFeito {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    BancoPrehFeito::class.java,
                    "partida-antiga"
                )
                    .createFromAsset("databases/database_reduzida.sqlite")
                    .allowMainThreadQueries()
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}