package com.example.partidasclassicas

import androidx.room.Dao
import androidx.room.Query

@Dao
interface MatchDAO {

    @Query("SELECT * FROM 'Match' WHERE id= :match_id")
    fun pegaPartidaDeID(match_id: Int): List<Match>

    @Query("SELECT DISTINCT season FROM 'Match'")
    fun listaTemporadas(): List<String>

    @Query("SELECT * FROM 'Match' WHERE (home_team_api_id= :equipeId OR away_team_api_id= :equipeId) AND season= :temporada ORDER BY stage")
    fun pegaPartidaComNomeETemporada(equipeId: Int, temporada: String): List<Match>
}