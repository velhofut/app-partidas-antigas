package com.example.partidasclassicas

import androidx.room.Dao
import androidx.room.Query

@Dao
interface TeamDAO {

    @Query("SELECT team_long_name FROM 'Team'")
    fun pegaTodosNomes(): List<String>

    @Query("SELECT team_long_name FROM 'Team' WHERE team_api_id= :team_api_id")
    fun pegaNomeDeApiId(team_api_id: Int?): String?

    @Query("SELECT team_api_id FROM 'Team' WHERE team_long_name= :team_long_name")
    fun pegaApiIDdeNome(team_long_name: String?): Int
}