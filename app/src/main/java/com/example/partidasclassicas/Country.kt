package com.example.partidasclassicas

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Country")
data class Country (
    @PrimaryKey(autoGenerate = false)
    val id: Int?,
    val name: String? = null
)