package com.example.partidasclassicas

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Player")
data class Player (
    @PrimaryKey(autoGenerate = false)
    val id: Int?,
    val player_api_id: Int? = null,
    val player_name: String? = null,
    val player_fifa_api_id: Int? = null,
    val birthday: String? = null,
    val height: Int? = null,
    val weight: Int? = null,
    )