package com.example.partidasclassicas

import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class PlacarAdapter (private val placaresEncontrados: List<Placar>): RecyclerView.Adapter<PlacarAdapter.ViewHolder>()  {
     inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         val nameHomeTeam: TextView = itemView.findViewById(R.id.name_home_team)
         val nameAwayTeam: TextView = itemView.findViewById(R.id.name_away_team)
         val homeGoals: TextView = itemView.findViewById(R.id.home_goals)
         val awayGoals: TextView = itemView.findViewById(R.id.away_goals)

     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val matchView = inflater.inflate(R.layout.match_card, parent, false)
        return ViewHolder(matchView)
    }

    override fun onBindViewHolder(holder: PlacarAdapter.ViewHolder, position: Int) {
        val placar: Placar = placaresEncontrados[position]
        val tvHomeTeam = holder.nameHomeTeam
        val tvAwayTeam = holder.nameAwayTeam
        val tvHomeGoals = holder.homeGoals
        val tvAwayGoals = holder.awayGoals
        tvHomeTeam.text = placar.homeTeamName.toString()
        tvAwayTeam.text = placar.awayTeamName.toString()
        tvHomeGoals.text = placar.homeTeamGoal.toString()
        tvAwayGoals.text = placar.awayTeamGoal.toString()
    }

    override fun getItemCount(): Int {
        return placaresEncontrados.size
    }
}