package com.example.partidasclassicas

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Match")
data class Match(
    @PrimaryKey(autoGenerate = false)
    val id: Int?,
    val country_id: Int? = null,
    val league_id: Int? = null,
    val season: String? = null,
    val stage: Int? = null,
    val date: String? = null,
    val match_api_id: Int? = null,
    val home_team_api_id: Int? = null,
    val away_team_api_id: Int? = null,
    val home_team_goal: Int? = null,
    val away_team_goal: Int? = null,
    val home_player_1: Int? = null,
    val home_player_2: Int? = null,
    val home_player_3: Int? = null,
    val home_player_4: Int? = null,
    val home_player_5: Int? = null,
    val home_player_6: Int? = null,
    val home_player_7: Int? = null,
    val home_player_8: Int? = null,
    val home_player_9: Int? = null,
    val home_player_10: Int? = null,
    val home_player_11: Int? = null,
    val away_player_1: Int? = null,
    val away_player_2: Int? = null,
    val away_player_3: Int? = null,
    val away_player_4: Int? = null,
    val away_player_5: Int? = null,
    val away_player_6: Int? = null,
    val away_player_7: Int? = null,
    val away_player_8: Int? = null,
    val away_player_9: Int? = null,
    val away_player_10: Int? = null,
    val away_player_11: Int? = null,

    )