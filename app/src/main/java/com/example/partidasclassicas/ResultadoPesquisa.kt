package com.example.partidasclassicas

import android.os.Bundle
import androidx.activity.ComponentActivity
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ResultadoPesquisa : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.resultado_activity)

        val RVPartidas = findViewById<View>(R.id.RV_partidas) as RecyclerView
        val nomeEquipe = intent.getStringExtra("Nome_Equipe").toString()
        val temporada = intent.getStringExtra("Temporada").toString()

        val bancoDados = BancoPrehFeito.getInstance(applicationContext)
        val equipeId = bancoDados.teamDao().pegaApiIDdeNome(nomeEquipe)
        val matchesEncontrados = bancoDados.matchDao().pegaPartidaComNomeETemporada(equipeId, temporada)
        val placaresEncontrados = listaMatchPraPlacar(bancoDados, matchesEncontrados)


        val adaptador = PlacarAdapter(placaresEncontrados)

        RVPartidas.adapter = adaptador
        RVPartidas.layoutManager = LinearLayoutManager(this)
    }

    private fun listaMatchPraPlacar(bancoDados: BancoPrehFeito, listaMatches: List<Match>): List<Placar> {
        val listaPlacares = mutableListOf<Placar>()
        for (match in listaMatches){
            val equipeMandante = bancoDados.teamDao().pegaNomeDeApiId(match.home_team_api_id)
            val equipeVisitante = bancoDados.teamDao().pegaNomeDeApiId(match.away_team_api_id)
            val placar = Placar(match.id, equipeMandante, equipeVisitante, match.home_team_goal, match.away_team_goal)
            listaPlacares.add(placar)
        }
        return listaPlacares
    }
}
