package com.example.partidasclassicas

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Team")
data class Team(
    @PrimaryKey(autoGenerate = false)
    val id: Int?,
    val team_api_id: Int? = null,
    val team_fifa_api_id: Int? = null,
    val team_long_name: String? = null,
    val team_short_name: String? = null,
)