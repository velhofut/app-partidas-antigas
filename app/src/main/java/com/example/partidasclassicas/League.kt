package com.example.partidasclassicas

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "League")
data class League (
    @PrimaryKey(autoGenerate = false)
    val id: Int?,
    val country_id: Int? = null,
    val name: String? = null,
)