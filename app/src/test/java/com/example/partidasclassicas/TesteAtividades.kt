package com.example.partidasclassicas
import android.content.Intent
import android.widget.Button
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.Shadows.shadowOf


@RunWith(RobolectricTestRunner::class)
class TesteAtividades {
    private lateinit var atividadePrincipal: MainActivity
    @Before
    fun configura() {
        atividadePrincipal = Robolectric.buildActivity(MainActivity::class.java).create().get()
    }

    @Test
    fun testaInicioPesquisa() {
        atividadePrincipal.findViewById<Button>(R.id.botao_inicio).performClick()
        val intentEsperado: Intent = Intent(atividadePrincipal, PesquisaActivity::class.java)
        val intentRecebido: Intent = shadowOf(RuntimeEnvironment.application).getNextStartedActivity()
        assertEquals(intentEsperado.component, intentRecebido.component)
    }
}


