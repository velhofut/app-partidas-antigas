package com.example.partidasclassicas

import android.content.Intent
import android.widget.AutoCompleteTextView
import android.widget.Button
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.Shadows

@RunWith(RobolectricTestRunner::class)
class TesteResultado {
    private lateinit var atividadePesquisa: PesquisaActivity
    @Before
    fun configura() {
        atividadePesquisa = Robolectric.buildActivity(PesquisaActivity::class.java).create().get()
    }

    @Test
    fun testaPesquisa() {
        atividadePesquisa.findViewById<Button>(R.id.botao_exibe_partidas).performClick()
        val intentEsperado: Intent = Intent(atividadePesquisa, ResultadoPesquisa::class.java)
        val intentRecebido: Intent = Shadows.shadowOf(RuntimeEnvironment.application).getNextStartedActivity()
        Assert.assertEquals(intentEsperado.component, intentRecebido.component)
    }
}