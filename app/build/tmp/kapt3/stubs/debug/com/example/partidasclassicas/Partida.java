package com.example.partidasclassicas;

@kotlin.Metadata(mv = {1, 9, 0}, k = 1, xi = 48, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u001a\u0018\u00002\u00020\u0001B]\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0007\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u000fJ\b\u0010\u001d\u001a\u00020\u0007H\u0016J\b\u0010\u001e\u001a\u00020\u0007H\u0016J\b\u0010\u001f\u001a\u00020\u0007H\u0016J\b\u0010 \u001a\u00020\u0007H\u0016R\u0011\u0010\f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0011R\u0011\u0010\t\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0011R\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0011R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0011R\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0015R\u0011\u0010\b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0011\u00a8\u0006!"}, d2 = {"Lcom/example/partidasclassicas/Partida;", "", "id", "", "country_id", "league_id", "season", "", "stage", "date", "match_api_id", "home_team_api_id", "away_team_api_id", "home_team_goal", "away_team_goal", "(IIILjava/lang/String;ILjava/lang/String;IIIII)V", "getAway_team_api_id", "()I", "getAway_team_goal", "getCountry_id", "getDate", "()Ljava/lang/String;", "getHome_team_api_id", "getHome_team_goal", "getId", "getLeague_id", "getMatch_api_id", "getSeason", "getStage", "awayTeamQuery", "countryQuery", "homeTeamQuery", "leagueQuery", "app_debug"})
public final class Partida {
    private final int id = 0;
    private final int country_id = 0;
    private final int league_id = 0;
    @org.jetbrains.annotations.NotNull
    private final java.lang.String season = null;
    private final int stage = 0;
    @org.jetbrains.annotations.NotNull
    private final java.lang.String date = null;
    private final int match_api_id = 0;
    private final int home_team_api_id = 0;
    private final int away_team_api_id = 0;
    private final int home_team_goal = 0;
    private final int away_team_goal = 0;
    
    public Partida(int id, int country_id, int league_id, @org.jetbrains.annotations.NotNull
    java.lang.String season, int stage, @org.jetbrains.annotations.NotNull
    java.lang.String date, int match_api_id, int home_team_api_id, int away_team_api_id, int home_team_goal, int away_team_goal) {
        super();
    }
    
    public final int getId() {
        return 0;
    }
    
    public final int getCountry_id() {
        return 0;
    }
    
    public final int getLeague_id() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getSeason() {
        return null;
    }
    
    public final int getStage() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getDate() {
        return null;
    }
    
    public final int getMatch_api_id() {
        return 0;
    }
    
    public final int getHome_team_api_id() {
        return 0;
    }
    
    public final int getAway_team_api_id() {
        return 0;
    }
    
    public final int getHome_team_goal() {
        return 0;
    }
    
    public final int getAway_team_goal() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    public java.lang.String homeTeamQuery() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public java.lang.String awayTeamQuery() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public java.lang.String leagueQuery() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public java.lang.String countryQuery() {
        return null;
    }
}