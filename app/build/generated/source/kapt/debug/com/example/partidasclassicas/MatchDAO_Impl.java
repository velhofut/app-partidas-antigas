package com.example.partidasclassicas;

import android.database.Cursor;
import androidx.annotation.NonNull;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class MatchDAO_Impl implements MatchDAO {
  private final RoomDatabase __db;

  public MatchDAO_Impl(@NonNull final RoomDatabase __db) {
    this.__db = __db;
  }

  @Override
  public List<Match> pegaPartidaDeID(final int match_id) {
    final String _sql = "SELECT * FROM 'Match' WHERE id= ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, match_id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCountryId = CursorUtil.getColumnIndexOrThrow(_cursor, "country_id");
      final int _cursorIndexOfLeagueId = CursorUtil.getColumnIndexOrThrow(_cursor, "league_id");
      final int _cursorIndexOfSeason = CursorUtil.getColumnIndexOrThrow(_cursor, "season");
      final int _cursorIndexOfStage = CursorUtil.getColumnIndexOrThrow(_cursor, "stage");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "date");
      final int _cursorIndexOfMatchApiId = CursorUtil.getColumnIndexOrThrow(_cursor, "match_api_id");
      final int _cursorIndexOfHomeTeamApiId = CursorUtil.getColumnIndexOrThrow(_cursor, "home_team_api_id");
      final int _cursorIndexOfAwayTeamApiId = CursorUtil.getColumnIndexOrThrow(_cursor, "away_team_api_id");
      final int _cursorIndexOfHomeTeamGoal = CursorUtil.getColumnIndexOrThrow(_cursor, "home_team_goal");
      final int _cursorIndexOfAwayTeamGoal = CursorUtil.getColumnIndexOrThrow(_cursor, "away_team_goal");
      final List<Match> _result = new ArrayList<Match>(_cursor.getCount());
      while (_cursor.moveToNext()) {
        final Match _item;
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        final int _tmpCountry_id;
        _tmpCountry_id = _cursor.getInt(_cursorIndexOfCountryId);
        final int _tmpLeague_id;
        _tmpLeague_id = _cursor.getInt(_cursorIndexOfLeagueId);
        final String _tmpSeason;
        if (_cursor.isNull(_cursorIndexOfSeason)) {
          _tmpSeason = null;
        } else {
          _tmpSeason = _cursor.getString(_cursorIndexOfSeason);
        }
        final int _tmpStage;
        _tmpStage = _cursor.getInt(_cursorIndexOfStage);
        final String _tmpDate;
        if (_cursor.isNull(_cursorIndexOfDate)) {
          _tmpDate = null;
        } else {
          _tmpDate = _cursor.getString(_cursorIndexOfDate);
        }
        final int _tmpMatch_api_id;
        _tmpMatch_api_id = _cursor.getInt(_cursorIndexOfMatchApiId);
        final int _tmpHome_team_api_id;
        _tmpHome_team_api_id = _cursor.getInt(_cursorIndexOfHomeTeamApiId);
        final int _tmpAway_team_api_id;
        _tmpAway_team_api_id = _cursor.getInt(_cursorIndexOfAwayTeamApiId);
        final int _tmpHome_team_goal;
        _tmpHome_team_goal = _cursor.getInt(_cursorIndexOfHomeTeamGoal);
        final int _tmpAway_team_goal;
        _tmpAway_team_goal = _cursor.getInt(_cursorIndexOfAwayTeamGoal);
        _item = new Match(_tmpId,_tmpCountry_id,_tmpLeague_id,_tmpSeason,_tmpStage,_tmpDate,_tmpMatch_api_id,_tmpHome_team_api_id,_tmpAway_team_api_id,_tmpHome_team_goal,_tmpAway_team_goal);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @NonNull
  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
