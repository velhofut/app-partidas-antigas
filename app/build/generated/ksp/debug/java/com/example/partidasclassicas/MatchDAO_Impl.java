package com.example.partidasclassicas;

import android.database.Cursor;
import androidx.annotation.NonNull;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated("androidx.room.RoomProcessor")
@SuppressWarnings({"unchecked", "deprecation"})
public final class MatchDAO_Impl implements MatchDAO {
  private final RoomDatabase __db;

  public MatchDAO_Impl(@NonNull final RoomDatabase __db) {
    this.__db = __db;
  }

  @Override
  public List<Match> pegaPartidaDeID(final int match_id) {
    final String _sql = "SELECT * FROM 'Match' WHERE id= ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, match_id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCountryId = CursorUtil.getColumnIndexOrThrow(_cursor, "country_id");
      final int _cursorIndexOfLeagueId = CursorUtil.getColumnIndexOrThrow(_cursor, "league_id");
      final int _cursorIndexOfSeason = CursorUtil.getColumnIndexOrThrow(_cursor, "season");
      final int _cursorIndexOfStage = CursorUtil.getColumnIndexOrThrow(_cursor, "stage");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "date");
      final int _cursorIndexOfMatchApiId = CursorUtil.getColumnIndexOrThrow(_cursor, "match_api_id");
      final int _cursorIndexOfHomeTeamApiId = CursorUtil.getColumnIndexOrThrow(_cursor, "home_team_api_id");
      final int _cursorIndexOfAwayTeamApiId = CursorUtil.getColumnIndexOrThrow(_cursor, "away_team_api_id");
      final int _cursorIndexOfHomeTeamGoal = CursorUtil.getColumnIndexOrThrow(_cursor, "home_team_goal");
      final int _cursorIndexOfAwayTeamGoal = CursorUtil.getColumnIndexOrThrow(_cursor, "away_team_goal");
      final int _cursorIndexOfHomePlayer1 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_1");
      final int _cursorIndexOfHomePlayer2 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_2");
      final int _cursorIndexOfHomePlayer3 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_3");
      final int _cursorIndexOfHomePlayer4 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_4");
      final int _cursorIndexOfHomePlayer5 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_5");
      final int _cursorIndexOfHomePlayer6 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_6");
      final int _cursorIndexOfHomePlayer7 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_7");
      final int _cursorIndexOfHomePlayer8 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_8");
      final int _cursorIndexOfHomePlayer9 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_9");
      final int _cursorIndexOfHomePlayer10 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_10");
      final int _cursorIndexOfHomePlayer11 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_11");
      final int _cursorIndexOfAwayPlayer1 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_1");
      final int _cursorIndexOfAwayPlayer2 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_2");
      final int _cursorIndexOfAwayPlayer3 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_3");
      final int _cursorIndexOfAwayPlayer4 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_4");
      final int _cursorIndexOfAwayPlayer5 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_5");
      final int _cursorIndexOfAwayPlayer6 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_6");
      final int _cursorIndexOfAwayPlayer7 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_7");
      final int _cursorIndexOfAwayPlayer8 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_8");
      final int _cursorIndexOfAwayPlayer9 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_9");
      final int _cursorIndexOfAwayPlayer10 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_10");
      final int _cursorIndexOfAwayPlayer11 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_11");
      final List<Match> _result = new ArrayList<Match>(_cursor.getCount());
      while (_cursor.moveToNext()) {
        final Match _item;
        final Integer _tmpId;
        if (_cursor.isNull(_cursorIndexOfId)) {
          _tmpId = null;
        } else {
          _tmpId = _cursor.getInt(_cursorIndexOfId);
        }
        final Integer _tmpCountry_id;
        if (_cursor.isNull(_cursorIndexOfCountryId)) {
          _tmpCountry_id = null;
        } else {
          _tmpCountry_id = _cursor.getInt(_cursorIndexOfCountryId);
        }
        final Integer _tmpLeague_id;
        if (_cursor.isNull(_cursorIndexOfLeagueId)) {
          _tmpLeague_id = null;
        } else {
          _tmpLeague_id = _cursor.getInt(_cursorIndexOfLeagueId);
        }
        final String _tmpSeason;
        if (_cursor.isNull(_cursorIndexOfSeason)) {
          _tmpSeason = null;
        } else {
          _tmpSeason = _cursor.getString(_cursorIndexOfSeason);
        }
        final Integer _tmpStage;
        if (_cursor.isNull(_cursorIndexOfStage)) {
          _tmpStage = null;
        } else {
          _tmpStage = _cursor.getInt(_cursorIndexOfStage);
        }
        final String _tmpDate;
        if (_cursor.isNull(_cursorIndexOfDate)) {
          _tmpDate = null;
        } else {
          _tmpDate = _cursor.getString(_cursorIndexOfDate);
        }
        final Integer _tmpMatch_api_id;
        if (_cursor.isNull(_cursorIndexOfMatchApiId)) {
          _tmpMatch_api_id = null;
        } else {
          _tmpMatch_api_id = _cursor.getInt(_cursorIndexOfMatchApiId);
        }
        final Integer _tmpHome_team_api_id;
        if (_cursor.isNull(_cursorIndexOfHomeTeamApiId)) {
          _tmpHome_team_api_id = null;
        } else {
          _tmpHome_team_api_id = _cursor.getInt(_cursorIndexOfHomeTeamApiId);
        }
        final Integer _tmpAway_team_api_id;
        if (_cursor.isNull(_cursorIndexOfAwayTeamApiId)) {
          _tmpAway_team_api_id = null;
        } else {
          _tmpAway_team_api_id = _cursor.getInt(_cursorIndexOfAwayTeamApiId);
        }
        final Integer _tmpHome_team_goal;
        if (_cursor.isNull(_cursorIndexOfHomeTeamGoal)) {
          _tmpHome_team_goal = null;
        } else {
          _tmpHome_team_goal = _cursor.getInt(_cursorIndexOfHomeTeamGoal);
        }
        final Integer _tmpAway_team_goal;
        if (_cursor.isNull(_cursorIndexOfAwayTeamGoal)) {
          _tmpAway_team_goal = null;
        } else {
          _tmpAway_team_goal = _cursor.getInt(_cursorIndexOfAwayTeamGoal);
        }
        final Integer _tmpHome_player_1;
        if (_cursor.isNull(_cursorIndexOfHomePlayer1)) {
          _tmpHome_player_1 = null;
        } else {
          _tmpHome_player_1 = _cursor.getInt(_cursorIndexOfHomePlayer1);
        }
        final Integer _tmpHome_player_2;
        if (_cursor.isNull(_cursorIndexOfHomePlayer2)) {
          _tmpHome_player_2 = null;
        } else {
          _tmpHome_player_2 = _cursor.getInt(_cursorIndexOfHomePlayer2);
        }
        final Integer _tmpHome_player_3;
        if (_cursor.isNull(_cursorIndexOfHomePlayer3)) {
          _tmpHome_player_3 = null;
        } else {
          _tmpHome_player_3 = _cursor.getInt(_cursorIndexOfHomePlayer3);
        }
        final Integer _tmpHome_player_4;
        if (_cursor.isNull(_cursorIndexOfHomePlayer4)) {
          _tmpHome_player_4 = null;
        } else {
          _tmpHome_player_4 = _cursor.getInt(_cursorIndexOfHomePlayer4);
        }
        final Integer _tmpHome_player_5;
        if (_cursor.isNull(_cursorIndexOfHomePlayer5)) {
          _tmpHome_player_5 = null;
        } else {
          _tmpHome_player_5 = _cursor.getInt(_cursorIndexOfHomePlayer5);
        }
        final Integer _tmpHome_player_6;
        if (_cursor.isNull(_cursorIndexOfHomePlayer6)) {
          _tmpHome_player_6 = null;
        } else {
          _tmpHome_player_6 = _cursor.getInt(_cursorIndexOfHomePlayer6);
        }
        final Integer _tmpHome_player_7;
        if (_cursor.isNull(_cursorIndexOfHomePlayer7)) {
          _tmpHome_player_7 = null;
        } else {
          _tmpHome_player_7 = _cursor.getInt(_cursorIndexOfHomePlayer7);
        }
        final Integer _tmpHome_player_8;
        if (_cursor.isNull(_cursorIndexOfHomePlayer8)) {
          _tmpHome_player_8 = null;
        } else {
          _tmpHome_player_8 = _cursor.getInt(_cursorIndexOfHomePlayer8);
        }
        final Integer _tmpHome_player_9;
        if (_cursor.isNull(_cursorIndexOfHomePlayer9)) {
          _tmpHome_player_9 = null;
        } else {
          _tmpHome_player_9 = _cursor.getInt(_cursorIndexOfHomePlayer9);
        }
        final Integer _tmpHome_player_10;
        if (_cursor.isNull(_cursorIndexOfHomePlayer10)) {
          _tmpHome_player_10 = null;
        } else {
          _tmpHome_player_10 = _cursor.getInt(_cursorIndexOfHomePlayer10);
        }
        final Integer _tmpHome_player_11;
        if (_cursor.isNull(_cursorIndexOfHomePlayer11)) {
          _tmpHome_player_11 = null;
        } else {
          _tmpHome_player_11 = _cursor.getInt(_cursorIndexOfHomePlayer11);
        }
        final Integer _tmpAway_player_1;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer1)) {
          _tmpAway_player_1 = null;
        } else {
          _tmpAway_player_1 = _cursor.getInt(_cursorIndexOfAwayPlayer1);
        }
        final Integer _tmpAway_player_2;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer2)) {
          _tmpAway_player_2 = null;
        } else {
          _tmpAway_player_2 = _cursor.getInt(_cursorIndexOfAwayPlayer2);
        }
        final Integer _tmpAway_player_3;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer3)) {
          _tmpAway_player_3 = null;
        } else {
          _tmpAway_player_3 = _cursor.getInt(_cursorIndexOfAwayPlayer3);
        }
        final Integer _tmpAway_player_4;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer4)) {
          _tmpAway_player_4 = null;
        } else {
          _tmpAway_player_4 = _cursor.getInt(_cursorIndexOfAwayPlayer4);
        }
        final Integer _tmpAway_player_5;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer5)) {
          _tmpAway_player_5 = null;
        } else {
          _tmpAway_player_5 = _cursor.getInt(_cursorIndexOfAwayPlayer5);
        }
        final Integer _tmpAway_player_6;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer6)) {
          _tmpAway_player_6 = null;
        } else {
          _tmpAway_player_6 = _cursor.getInt(_cursorIndexOfAwayPlayer6);
        }
        final Integer _tmpAway_player_7;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer7)) {
          _tmpAway_player_7 = null;
        } else {
          _tmpAway_player_7 = _cursor.getInt(_cursorIndexOfAwayPlayer7);
        }
        final Integer _tmpAway_player_8;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer8)) {
          _tmpAway_player_8 = null;
        } else {
          _tmpAway_player_8 = _cursor.getInt(_cursorIndexOfAwayPlayer8);
        }
        final Integer _tmpAway_player_9;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer9)) {
          _tmpAway_player_9 = null;
        } else {
          _tmpAway_player_9 = _cursor.getInt(_cursorIndexOfAwayPlayer9);
        }
        final Integer _tmpAway_player_10;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer10)) {
          _tmpAway_player_10 = null;
        } else {
          _tmpAway_player_10 = _cursor.getInt(_cursorIndexOfAwayPlayer10);
        }
        final Integer _tmpAway_player_11;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer11)) {
          _tmpAway_player_11 = null;
        } else {
          _tmpAway_player_11 = _cursor.getInt(_cursorIndexOfAwayPlayer11);
        }
        _item = new Match(_tmpId,_tmpCountry_id,_tmpLeague_id,_tmpSeason,_tmpStage,_tmpDate,_tmpMatch_api_id,_tmpHome_team_api_id,_tmpAway_team_api_id,_tmpHome_team_goal,_tmpAway_team_goal,_tmpHome_player_1,_tmpHome_player_2,_tmpHome_player_3,_tmpHome_player_4,_tmpHome_player_5,_tmpHome_player_6,_tmpHome_player_7,_tmpHome_player_8,_tmpHome_player_9,_tmpHome_player_10,_tmpHome_player_11,_tmpAway_player_1,_tmpAway_player_2,_tmpAway_player_3,_tmpAway_player_4,_tmpAway_player_5,_tmpAway_player_6,_tmpAway_player_7,_tmpAway_player_8,_tmpAway_player_9,_tmpAway_player_10,_tmpAway_player_11);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<String> listaTemporadas() {
    final String _sql = "SELECT DISTINCT season FROM 'Match'";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final List<String> _result = new ArrayList<String>(_cursor.getCount());
      while (_cursor.moveToNext()) {
        final String _item;
        _item = _cursor.getString(0);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<Match> pegaPartidaComNomeETemporada(final int equipeId, final String temporada) {
    final String _sql = "SELECT * FROM 'Match' WHERE (home_team_api_id= ? OR away_team_api_id= ?) AND season= ? ORDER BY stage";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 3);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, equipeId);
    _argIndex = 2;
    _statement.bindLong(_argIndex, equipeId);
    _argIndex = 3;
    _statement.bindString(_argIndex, temporada);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCountryId = CursorUtil.getColumnIndexOrThrow(_cursor, "country_id");
      final int _cursorIndexOfLeagueId = CursorUtil.getColumnIndexOrThrow(_cursor, "league_id");
      final int _cursorIndexOfSeason = CursorUtil.getColumnIndexOrThrow(_cursor, "season");
      final int _cursorIndexOfStage = CursorUtil.getColumnIndexOrThrow(_cursor, "stage");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "date");
      final int _cursorIndexOfMatchApiId = CursorUtil.getColumnIndexOrThrow(_cursor, "match_api_id");
      final int _cursorIndexOfHomeTeamApiId = CursorUtil.getColumnIndexOrThrow(_cursor, "home_team_api_id");
      final int _cursorIndexOfAwayTeamApiId = CursorUtil.getColumnIndexOrThrow(_cursor, "away_team_api_id");
      final int _cursorIndexOfHomeTeamGoal = CursorUtil.getColumnIndexOrThrow(_cursor, "home_team_goal");
      final int _cursorIndexOfAwayTeamGoal = CursorUtil.getColumnIndexOrThrow(_cursor, "away_team_goal");
      final int _cursorIndexOfHomePlayer1 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_1");
      final int _cursorIndexOfHomePlayer2 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_2");
      final int _cursorIndexOfHomePlayer3 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_3");
      final int _cursorIndexOfHomePlayer4 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_4");
      final int _cursorIndexOfHomePlayer5 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_5");
      final int _cursorIndexOfHomePlayer6 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_6");
      final int _cursorIndexOfHomePlayer7 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_7");
      final int _cursorIndexOfHomePlayer8 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_8");
      final int _cursorIndexOfHomePlayer9 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_9");
      final int _cursorIndexOfHomePlayer10 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_10");
      final int _cursorIndexOfHomePlayer11 = CursorUtil.getColumnIndexOrThrow(_cursor, "home_player_11");
      final int _cursorIndexOfAwayPlayer1 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_1");
      final int _cursorIndexOfAwayPlayer2 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_2");
      final int _cursorIndexOfAwayPlayer3 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_3");
      final int _cursorIndexOfAwayPlayer4 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_4");
      final int _cursorIndexOfAwayPlayer5 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_5");
      final int _cursorIndexOfAwayPlayer6 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_6");
      final int _cursorIndexOfAwayPlayer7 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_7");
      final int _cursorIndexOfAwayPlayer8 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_8");
      final int _cursorIndexOfAwayPlayer9 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_9");
      final int _cursorIndexOfAwayPlayer10 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_10");
      final int _cursorIndexOfAwayPlayer11 = CursorUtil.getColumnIndexOrThrow(_cursor, "away_player_11");
      final List<Match> _result = new ArrayList<Match>(_cursor.getCount());
      while (_cursor.moveToNext()) {
        final Match _item;
        final Integer _tmpId;
        if (_cursor.isNull(_cursorIndexOfId)) {
          _tmpId = null;
        } else {
          _tmpId = _cursor.getInt(_cursorIndexOfId);
        }
        final Integer _tmpCountry_id;
        if (_cursor.isNull(_cursorIndexOfCountryId)) {
          _tmpCountry_id = null;
        } else {
          _tmpCountry_id = _cursor.getInt(_cursorIndexOfCountryId);
        }
        final Integer _tmpLeague_id;
        if (_cursor.isNull(_cursorIndexOfLeagueId)) {
          _tmpLeague_id = null;
        } else {
          _tmpLeague_id = _cursor.getInt(_cursorIndexOfLeagueId);
        }
        final String _tmpSeason;
        if (_cursor.isNull(_cursorIndexOfSeason)) {
          _tmpSeason = null;
        } else {
          _tmpSeason = _cursor.getString(_cursorIndexOfSeason);
        }
        final Integer _tmpStage;
        if (_cursor.isNull(_cursorIndexOfStage)) {
          _tmpStage = null;
        } else {
          _tmpStage = _cursor.getInt(_cursorIndexOfStage);
        }
        final String _tmpDate;
        if (_cursor.isNull(_cursorIndexOfDate)) {
          _tmpDate = null;
        } else {
          _tmpDate = _cursor.getString(_cursorIndexOfDate);
        }
        final Integer _tmpMatch_api_id;
        if (_cursor.isNull(_cursorIndexOfMatchApiId)) {
          _tmpMatch_api_id = null;
        } else {
          _tmpMatch_api_id = _cursor.getInt(_cursorIndexOfMatchApiId);
        }
        final Integer _tmpHome_team_api_id;
        if (_cursor.isNull(_cursorIndexOfHomeTeamApiId)) {
          _tmpHome_team_api_id = null;
        } else {
          _tmpHome_team_api_id = _cursor.getInt(_cursorIndexOfHomeTeamApiId);
        }
        final Integer _tmpAway_team_api_id;
        if (_cursor.isNull(_cursorIndexOfAwayTeamApiId)) {
          _tmpAway_team_api_id = null;
        } else {
          _tmpAway_team_api_id = _cursor.getInt(_cursorIndexOfAwayTeamApiId);
        }
        final Integer _tmpHome_team_goal;
        if (_cursor.isNull(_cursorIndexOfHomeTeamGoal)) {
          _tmpHome_team_goal = null;
        } else {
          _tmpHome_team_goal = _cursor.getInt(_cursorIndexOfHomeTeamGoal);
        }
        final Integer _tmpAway_team_goal;
        if (_cursor.isNull(_cursorIndexOfAwayTeamGoal)) {
          _tmpAway_team_goal = null;
        } else {
          _tmpAway_team_goal = _cursor.getInt(_cursorIndexOfAwayTeamGoal);
        }
        final Integer _tmpHome_player_1;
        if (_cursor.isNull(_cursorIndexOfHomePlayer1)) {
          _tmpHome_player_1 = null;
        } else {
          _tmpHome_player_1 = _cursor.getInt(_cursorIndexOfHomePlayer1);
        }
        final Integer _tmpHome_player_2;
        if (_cursor.isNull(_cursorIndexOfHomePlayer2)) {
          _tmpHome_player_2 = null;
        } else {
          _tmpHome_player_2 = _cursor.getInt(_cursorIndexOfHomePlayer2);
        }
        final Integer _tmpHome_player_3;
        if (_cursor.isNull(_cursorIndexOfHomePlayer3)) {
          _tmpHome_player_3 = null;
        } else {
          _tmpHome_player_3 = _cursor.getInt(_cursorIndexOfHomePlayer3);
        }
        final Integer _tmpHome_player_4;
        if (_cursor.isNull(_cursorIndexOfHomePlayer4)) {
          _tmpHome_player_4 = null;
        } else {
          _tmpHome_player_4 = _cursor.getInt(_cursorIndexOfHomePlayer4);
        }
        final Integer _tmpHome_player_5;
        if (_cursor.isNull(_cursorIndexOfHomePlayer5)) {
          _tmpHome_player_5 = null;
        } else {
          _tmpHome_player_5 = _cursor.getInt(_cursorIndexOfHomePlayer5);
        }
        final Integer _tmpHome_player_6;
        if (_cursor.isNull(_cursorIndexOfHomePlayer6)) {
          _tmpHome_player_6 = null;
        } else {
          _tmpHome_player_6 = _cursor.getInt(_cursorIndexOfHomePlayer6);
        }
        final Integer _tmpHome_player_7;
        if (_cursor.isNull(_cursorIndexOfHomePlayer7)) {
          _tmpHome_player_7 = null;
        } else {
          _tmpHome_player_7 = _cursor.getInt(_cursorIndexOfHomePlayer7);
        }
        final Integer _tmpHome_player_8;
        if (_cursor.isNull(_cursorIndexOfHomePlayer8)) {
          _tmpHome_player_8 = null;
        } else {
          _tmpHome_player_8 = _cursor.getInt(_cursorIndexOfHomePlayer8);
        }
        final Integer _tmpHome_player_9;
        if (_cursor.isNull(_cursorIndexOfHomePlayer9)) {
          _tmpHome_player_9 = null;
        } else {
          _tmpHome_player_9 = _cursor.getInt(_cursorIndexOfHomePlayer9);
        }
        final Integer _tmpHome_player_10;
        if (_cursor.isNull(_cursorIndexOfHomePlayer10)) {
          _tmpHome_player_10 = null;
        } else {
          _tmpHome_player_10 = _cursor.getInt(_cursorIndexOfHomePlayer10);
        }
        final Integer _tmpHome_player_11;
        if (_cursor.isNull(_cursorIndexOfHomePlayer11)) {
          _tmpHome_player_11 = null;
        } else {
          _tmpHome_player_11 = _cursor.getInt(_cursorIndexOfHomePlayer11);
        }
        final Integer _tmpAway_player_1;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer1)) {
          _tmpAway_player_1 = null;
        } else {
          _tmpAway_player_1 = _cursor.getInt(_cursorIndexOfAwayPlayer1);
        }
        final Integer _tmpAway_player_2;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer2)) {
          _tmpAway_player_2 = null;
        } else {
          _tmpAway_player_2 = _cursor.getInt(_cursorIndexOfAwayPlayer2);
        }
        final Integer _tmpAway_player_3;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer3)) {
          _tmpAway_player_3 = null;
        } else {
          _tmpAway_player_3 = _cursor.getInt(_cursorIndexOfAwayPlayer3);
        }
        final Integer _tmpAway_player_4;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer4)) {
          _tmpAway_player_4 = null;
        } else {
          _tmpAway_player_4 = _cursor.getInt(_cursorIndexOfAwayPlayer4);
        }
        final Integer _tmpAway_player_5;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer5)) {
          _tmpAway_player_5 = null;
        } else {
          _tmpAway_player_5 = _cursor.getInt(_cursorIndexOfAwayPlayer5);
        }
        final Integer _tmpAway_player_6;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer6)) {
          _tmpAway_player_6 = null;
        } else {
          _tmpAway_player_6 = _cursor.getInt(_cursorIndexOfAwayPlayer6);
        }
        final Integer _tmpAway_player_7;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer7)) {
          _tmpAway_player_7 = null;
        } else {
          _tmpAway_player_7 = _cursor.getInt(_cursorIndexOfAwayPlayer7);
        }
        final Integer _tmpAway_player_8;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer8)) {
          _tmpAway_player_8 = null;
        } else {
          _tmpAway_player_8 = _cursor.getInt(_cursorIndexOfAwayPlayer8);
        }
        final Integer _tmpAway_player_9;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer9)) {
          _tmpAway_player_9 = null;
        } else {
          _tmpAway_player_9 = _cursor.getInt(_cursorIndexOfAwayPlayer9);
        }
        final Integer _tmpAway_player_10;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer10)) {
          _tmpAway_player_10 = null;
        } else {
          _tmpAway_player_10 = _cursor.getInt(_cursorIndexOfAwayPlayer10);
        }
        final Integer _tmpAway_player_11;
        if (_cursor.isNull(_cursorIndexOfAwayPlayer11)) {
          _tmpAway_player_11 = null;
        } else {
          _tmpAway_player_11 = _cursor.getInt(_cursorIndexOfAwayPlayer11);
        }
        _item = new Match(_tmpId,_tmpCountry_id,_tmpLeague_id,_tmpSeason,_tmpStage,_tmpDate,_tmpMatch_api_id,_tmpHome_team_api_id,_tmpAway_team_api_id,_tmpHome_team_goal,_tmpAway_team_goal,_tmpHome_player_1,_tmpHome_player_2,_tmpHome_player_3,_tmpHome_player_4,_tmpHome_player_5,_tmpHome_player_6,_tmpHome_player_7,_tmpHome_player_8,_tmpHome_player_9,_tmpHome_player_10,_tmpHome_player_11,_tmpAway_player_1,_tmpAway_player_2,_tmpAway_player_3,_tmpAway_player_4,_tmpAway_player_5,_tmpAway_player_6,_tmpAway_player_7,_tmpAway_player_8,_tmpAway_player_9,_tmpAway_player_10,_tmpAway_player_11);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @NonNull
  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
