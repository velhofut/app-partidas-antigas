package com.example.partidasclassicas;

import androidx.annotation.NonNull;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.room.RoomOpenHelper;
import androidx.room.migration.AutoMigrationSpec;
import androidx.room.migration.Migration;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.Generated;

@Generated("androidx.room.RoomProcessor")
@SuppressWarnings({"unchecked", "deprecation"})
public final class BancoPrehFeito_Impl extends BancoPrehFeito {
  private volatile MatchDAO _matchDAO;

  private volatile TeamDAO _teamDAO;

  @Override
  @NonNull
  protected SupportSQLiteOpenHelper createOpenHelper(@NonNull final DatabaseConfiguration config) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(config, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(@NonNull final SupportSQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS `Match` (`id` INTEGER, `country_id` INTEGER, `league_id` INTEGER, `season` TEXT, `stage` INTEGER, `date` TEXT, `match_api_id` INTEGER, `home_team_api_id` INTEGER, `away_team_api_id` INTEGER, `home_team_goal` INTEGER, `away_team_goal` INTEGER, `home_player_1` INTEGER, `home_player_2` INTEGER, `home_player_3` INTEGER, `home_player_4` INTEGER, `home_player_5` INTEGER, `home_player_6` INTEGER, `home_player_7` INTEGER, `home_player_8` INTEGER, `home_player_9` INTEGER, `home_player_10` INTEGER, `home_player_11` INTEGER, `away_player_1` INTEGER, `away_player_2` INTEGER, `away_player_3` INTEGER, `away_player_4` INTEGER, `away_player_5` INTEGER, `away_player_6` INTEGER, `away_player_7` INTEGER, `away_player_8` INTEGER, `away_player_9` INTEGER, `away_player_10` INTEGER, `away_player_11` INTEGER, PRIMARY KEY(`id`))");
        db.execSQL("CREATE TABLE IF NOT EXISTS `Team` (`id` INTEGER, `team_api_id` INTEGER, `team_fifa_api_id` INTEGER, `team_long_name` TEXT, `team_short_name` TEXT, PRIMARY KEY(`id`))");
        db.execSQL("CREATE TABLE IF NOT EXISTS `Player` (`id` INTEGER, `player_api_id` INTEGER, `player_name` TEXT, `player_fifa_api_id` INTEGER, `birthday` TEXT, `height` INTEGER, `weight` INTEGER, PRIMARY KEY(`id`))");
        db.execSQL("CREATE TABLE IF NOT EXISTS `Country` (`id` INTEGER, `name` TEXT, PRIMARY KEY(`id`))");
        db.execSQL("CREATE TABLE IF NOT EXISTS `League` (`id` INTEGER, `country_id` INTEGER, `name` TEXT, PRIMARY KEY(`id`))");
        db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '967c4515d6d3485e77f1d487681f5ac6')");
      }

      @Override
      public void dropAllTables(@NonNull final SupportSQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS `Match`");
        db.execSQL("DROP TABLE IF EXISTS `Team`");
        db.execSQL("DROP TABLE IF EXISTS `Player`");
        db.execSQL("DROP TABLE IF EXISTS `Country`");
        db.execSQL("DROP TABLE IF EXISTS `League`");
        final List<? extends RoomDatabase.Callback> _callbacks = mCallbacks;
        if (_callbacks != null) {
          for (RoomDatabase.Callback _callback : _callbacks) {
            _callback.onDestructiveMigration(db);
          }
        }
      }

      @Override
      public void onCreate(@NonNull final SupportSQLiteDatabase db) {
        final List<? extends RoomDatabase.Callback> _callbacks = mCallbacks;
        if (_callbacks != null) {
          for (RoomDatabase.Callback _callback : _callbacks) {
            _callback.onCreate(db);
          }
        }
      }

      @Override
      public void onOpen(@NonNull final SupportSQLiteDatabase db) {
        mDatabase = db;
        internalInitInvalidationTracker(db);
        final List<? extends RoomDatabase.Callback> _callbacks = mCallbacks;
        if (_callbacks != null) {
          for (RoomDatabase.Callback _callback : _callbacks) {
            _callback.onOpen(db);
          }
        }
      }

      @Override
      public void onPreMigrate(@NonNull final SupportSQLiteDatabase db) {
        DBUtil.dropFtsSyncTriggers(db);
      }

      @Override
      public void onPostMigrate(@NonNull final SupportSQLiteDatabase db) {
      }

      @Override
      @NonNull
      public RoomOpenHelper.ValidationResult onValidateSchema(
          @NonNull final SupportSQLiteDatabase db) {
        final HashMap<String, TableInfo.Column> _columnsMatch = new HashMap<String, TableInfo.Column>(33);
        _columnsMatch.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("country_id", new TableInfo.Column("country_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("league_id", new TableInfo.Column("league_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("season", new TableInfo.Column("season", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("stage", new TableInfo.Column("stage", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("date", new TableInfo.Column("date", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("match_api_id", new TableInfo.Column("match_api_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_team_api_id", new TableInfo.Column("home_team_api_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_team_api_id", new TableInfo.Column("away_team_api_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_team_goal", new TableInfo.Column("home_team_goal", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_team_goal", new TableInfo.Column("away_team_goal", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_player_1", new TableInfo.Column("home_player_1", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_player_2", new TableInfo.Column("home_player_2", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_player_3", new TableInfo.Column("home_player_3", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_player_4", new TableInfo.Column("home_player_4", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_player_5", new TableInfo.Column("home_player_5", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_player_6", new TableInfo.Column("home_player_6", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_player_7", new TableInfo.Column("home_player_7", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_player_8", new TableInfo.Column("home_player_8", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_player_9", new TableInfo.Column("home_player_9", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_player_10", new TableInfo.Column("home_player_10", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("home_player_11", new TableInfo.Column("home_player_11", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_player_1", new TableInfo.Column("away_player_1", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_player_2", new TableInfo.Column("away_player_2", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_player_3", new TableInfo.Column("away_player_3", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_player_4", new TableInfo.Column("away_player_4", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_player_5", new TableInfo.Column("away_player_5", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_player_6", new TableInfo.Column("away_player_6", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_player_7", new TableInfo.Column("away_player_7", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_player_8", new TableInfo.Column("away_player_8", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_player_9", new TableInfo.Column("away_player_9", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_player_10", new TableInfo.Column("away_player_10", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMatch.put("away_player_11", new TableInfo.Column("away_player_11", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysMatch = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesMatch = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoMatch = new TableInfo("Match", _columnsMatch, _foreignKeysMatch, _indicesMatch);
        final TableInfo _existingMatch = TableInfo.read(db, "Match");
        if (!_infoMatch.equals(_existingMatch)) {
          return new RoomOpenHelper.ValidationResult(false, "Match(com.example.partidasclassicas.Match).\n"
                  + " Expected:\n" + _infoMatch + "\n"
                  + " Found:\n" + _existingMatch);
        }
        final HashMap<String, TableInfo.Column> _columnsTeam = new HashMap<String, TableInfo.Column>(5);
        _columnsTeam.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTeam.put("team_api_id", new TableInfo.Column("team_api_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTeam.put("team_fifa_api_id", new TableInfo.Column("team_fifa_api_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTeam.put("team_long_name", new TableInfo.Column("team_long_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTeam.put("team_short_name", new TableInfo.Column("team_short_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysTeam = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesTeam = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoTeam = new TableInfo("Team", _columnsTeam, _foreignKeysTeam, _indicesTeam);
        final TableInfo _existingTeam = TableInfo.read(db, "Team");
        if (!_infoTeam.equals(_existingTeam)) {
          return new RoomOpenHelper.ValidationResult(false, "Team(com.example.partidasclassicas.Team).\n"
                  + " Expected:\n" + _infoTeam + "\n"
                  + " Found:\n" + _existingTeam);
        }
        final HashMap<String, TableInfo.Column> _columnsPlayer = new HashMap<String, TableInfo.Column>(7);
        _columnsPlayer.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsPlayer.put("player_api_id", new TableInfo.Column("player_api_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsPlayer.put("player_name", new TableInfo.Column("player_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsPlayer.put("player_fifa_api_id", new TableInfo.Column("player_fifa_api_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsPlayer.put("birthday", new TableInfo.Column("birthday", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsPlayer.put("height", new TableInfo.Column("height", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsPlayer.put("weight", new TableInfo.Column("weight", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysPlayer = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesPlayer = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoPlayer = new TableInfo("Player", _columnsPlayer, _foreignKeysPlayer, _indicesPlayer);
        final TableInfo _existingPlayer = TableInfo.read(db, "Player");
        if (!_infoPlayer.equals(_existingPlayer)) {
          return new RoomOpenHelper.ValidationResult(false, "Player(com.example.partidasclassicas.Player).\n"
                  + " Expected:\n" + _infoPlayer + "\n"
                  + " Found:\n" + _existingPlayer);
        }
        final HashMap<String, TableInfo.Column> _columnsCountry = new HashMap<String, TableInfo.Column>(2);
        _columnsCountry.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCountry.put("name", new TableInfo.Column("name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysCountry = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesCountry = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoCountry = new TableInfo("Country", _columnsCountry, _foreignKeysCountry, _indicesCountry);
        final TableInfo _existingCountry = TableInfo.read(db, "Country");
        if (!_infoCountry.equals(_existingCountry)) {
          return new RoomOpenHelper.ValidationResult(false, "Country(com.example.partidasclassicas.Country).\n"
                  + " Expected:\n" + _infoCountry + "\n"
                  + " Found:\n" + _existingCountry);
        }
        final HashMap<String, TableInfo.Column> _columnsLeague = new HashMap<String, TableInfo.Column>(3);
        _columnsLeague.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsLeague.put("country_id", new TableInfo.Column("country_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsLeague.put("name", new TableInfo.Column("name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysLeague = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesLeague = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoLeague = new TableInfo("League", _columnsLeague, _foreignKeysLeague, _indicesLeague);
        final TableInfo _existingLeague = TableInfo.read(db, "League");
        if (!_infoLeague.equals(_existingLeague)) {
          return new RoomOpenHelper.ValidationResult(false, "League(com.example.partidasclassicas.League).\n"
                  + " Expected:\n" + _infoLeague + "\n"
                  + " Found:\n" + _existingLeague);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "967c4515d6d3485e77f1d487681f5ac6", "7e46ba3001cf1fb2498b85e4b4b62dcd");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(config.context).name(config.name).callback(_openCallback).build();
    final SupportSQLiteOpenHelper _helper = config.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  @NonNull
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    final HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "Match","Team","Player","Country","League");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `Match`");
      _db.execSQL("DELETE FROM `Team`");
      _db.execSQL("DELETE FROM `Player`");
      _db.execSQL("DELETE FROM `Country`");
      _db.execSQL("DELETE FROM `League`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  @NonNull
  protected Map<Class<?>, List<Class<?>>> getRequiredTypeConverters() {
    final HashMap<Class<?>, List<Class<?>>> _typeConvertersMap = new HashMap<Class<?>, List<Class<?>>>();
    _typeConvertersMap.put(MatchDAO.class, MatchDAO_Impl.getRequiredConverters());
    _typeConvertersMap.put(TeamDAO.class, TeamDAO_Impl.getRequiredConverters());
    return _typeConvertersMap;
  }

  @Override
  @NonNull
  public Set<Class<? extends AutoMigrationSpec>> getRequiredAutoMigrationSpecs() {
    final HashSet<Class<? extends AutoMigrationSpec>> _autoMigrationSpecsSet = new HashSet<Class<? extends AutoMigrationSpec>>();
    return _autoMigrationSpecsSet;
  }

  @Override
  @NonNull
  public List<Migration> getAutoMigrations(
      @NonNull final Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> autoMigrationSpecs) {
    final List<Migration> _autoMigrations = new ArrayList<Migration>();
    return _autoMigrations;
  }

  @Override
  public MatchDAO matchDao() {
    if (_matchDAO != null) {
      return _matchDAO;
    } else {
      synchronized(this) {
        if(_matchDAO == null) {
          _matchDAO = new MatchDAO_Impl(this);
        }
        return _matchDAO;
      }
    }
  }

  @Override
  public TeamDAO teamDao() {
    if (_teamDAO != null) {
      return _teamDAO;
    } else {
      synchronized(this) {
        if(_teamDAO == null) {
          _teamDAO = new TeamDAO_Impl(this);
        }
        return _teamDAO;
      }
    }
  }
}
