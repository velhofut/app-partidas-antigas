# ListLeague

Esse é um app que permite ao usuário ver todas as partidas de um clube de futebol em sua liga nacional entre os anos
de 2008 a 2016.

Ele funciona usando um banco de dados persistente acoplado ao app.

## Como Usar

Para executar o app, é necessário um emulador de Android. O Android Studio fornece esse emulador, apesar de existirem também outras ferramentas.

Ao executar o app, surgirá uma tela com um único botão: iniciar pesquisa.

Ao clicar nele, o usuário será levado a outra tela, onde pode preencher dois campos.

No primeiro, escreve-se o nome de uma equipe e uma lista dropdown de preenchimento sugere as equipes disponíveis.

Basta clicar numa das equipes que surgiram.

No segundo campo, digita-se algum ano e a lista dropdown sugere as temporadas disponíveis.

Basta clicar em alguma das temporadas disponíveis.

Por fim, ao clicar em "Pesquisar Partidas", uma nova tela surgirá com as partidas do time na liga nacional daquela temporada.

## Ligas e Temporadas Disponíveis

| Ligas                     |
| --------------------------|
| Belgium Jupiler League    |
| England Premier League    |
| France Ligue 1            |
| Germany 1. Bundesliga     |
| Italy Serie A             |
| Netherlands Eredivisie    |
| Poland Ekstraklasa        |
| Portugal Liga ZON Sagres  |
| Scotland Premier League   |
| Spain LIGA BBVA           |
| Switzerland Super League  |

| Temporadas |
| ---------  |
| 2008/2009  |
| 2009/2010  |
| 2010/2011  |
| 2011/2012  |
| 2012/2013  |
| 2013/2014  |
| 2014/2015  |
| 2015/2016  |